import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

extension StatelessWidgetExtension on StatelessWidget {
  String randomImage() {
    final random = Random();
    return 'https://via.placeholder.com/${64 + random.nextInt(512)}x${64 + random.nextInt(512)}/${random.nextInt(256).toRadixString(16)}${random.nextInt(256).toRadixString(16)}${random.nextInt(256).toRadixString(16)}';
  }

  void showConfirmationAlert(
    BuildContext context, {
    String title = '',
    String content = '',
    String? cancelButton,
    String? submitButton,
    void Function(bool submit)? onPop,
  }) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: Text(content),
            actions: [
              if (cancelButton != null)
                ElevatedButton(
                  child: Text(cancelButton, style: TextStyle(color: Colors.white)),
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                    onPop?.call(false);
                  },
                ),
              if (submitButton != null)
                ElevatedButton(
                  child: Text(submitButton, style: TextStyle(color: Colors.white)),
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                    onPop?.call(true);
                  },
                ),
            ],
          );
        });
  }

  void showAlert(
    BuildContext context, {
    String title = '',
    String content = '',
    String? closeButton,
    VoidCallback? onPop,
  }) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: Text(content),
            actions: [
              if (closeButton != null)
                ElevatedButton(
                  child: Text(closeButton, style: TextStyle(color: Colors.white)),
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop();
                    onPop?.call();
                  },
                ),
            ],
          );
        });
  }
}
