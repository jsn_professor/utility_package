import 'package:flutter/material.dart';

extension ColorExtension on Color {
  MaterialColor get materialColor {
    List<double> shades = [.05];
    for (int i = 1; i < 10; i++) {
      shades.add(0.1 * i);
    }
    Map<int, Color> swatch = {};
    final int r = red, g = green, b = blue;
    for (final shade in shades) {
      final double delta = 0.5 - shade;
      swatch[(shade * 1000).round()] = Color.fromRGBO(
        r + ((delta < 0 ? r : (255 - r)) * delta).round(),
        g + ((delta < 0 ? g : (255 - g)) * delta).round(),
        b + ((delta < 0 ? b : (255 - b)) * delta).round(),
        1,
      );
    }
    return MaterialColor(value, swatch);
  }
}
