import 'package:flutter/material.dart';

class Retry extends StatelessWidget {
  final Widget? child;
  final VoidCallback? onPressed;

  const Retry({Key? key, this.child, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _child = child;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        IconButton(
          icon: Icon(
            Icons.refresh,
          ),
          onPressed: onPressed,
        ),
        if (_child != null) _child,
      ],
    );
  }
}
