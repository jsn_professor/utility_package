import 'package:flutter/material.dart';

class SliverTitle extends StatefulWidget {
  final Widget child;

  const SliverTitle(this.child, {Key? key}) : super(key: key);

  @override
  _SliverTitleState createState() {
    return _SliverTitleState();
  }
}

class _SliverTitleState extends State<SliverTitle> {
  FlexibleSpaceBarSettings? _settings;
  ScrollPosition? _position;
  var _visible = true;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _removeListener();
    _addListener();
  }

  @override
  void dispose() {
    _removeListener();
    super.dispose();
  }

  void _addListener() {
    _settings = context.dependOnInheritedWidgetOfExactType(aspect: FlexibleSpaceBarSettings);
    _position = Scrollable.of(context)?.position;
    _position?.addListener(_positionListener);
    _positionListener();
  }

  void _removeListener() {
    _position?.removeListener(_positionListener);
  }

  void _positionListener() {
    final settings = _settings;
    bool visible = settings == null || settings.currentExtent <= settings.minExtent;
    if (_visible != visible) {
      setState(() {
        _visible = visible;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _settings?.minExtent,
      child: AnimatedOpacity(
        duration: const Duration(milliseconds: 300),
        opacity: _visible ? 1 : 0,
        child: widget.child,
      ),
    );
  }
}
