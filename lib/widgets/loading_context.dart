import 'package:flutter/material.dart';

class LoadingContext extends InheritedWidget {
  final Future? future;
  final ValueChanged<Future> onFutureChanged;

  const LoadingContext({
    this.future,
    required this.onFutureChanged,
    required Widget child,
  }) : super(child: child);

  static LoadingContext of(BuildContext context) {
    return context.getElementForInheritedWidgetOfExactType<LoadingContext>()!.widget as LoadingContext;
  }

  @override
  bool updateShouldNotify(LoadingContext oldWidget) {
    return false;
  }
}

class LoadingContextWidget extends StatefulWidget {
  final double strokeWidth;
  final Color? backgroundColor;
  final Animation<Color>? valueColor;
  final Widget child;

  const LoadingContextWidget({Key? key, this.strokeWidth = 4, this.backgroundColor, this.valueColor, required this.child}) : super(key: key);

  @override
  _LoadingContextWidgetState createState() => _LoadingContextWidgetState();
}

class _LoadingContextWidgetState extends State<LoadingContextWidget> {
  Future? _future;

  @override
  Widget build(BuildContext context) {
    return LoadingContext(
      future: _future,
      onFutureChanged: _onFutureChanged,
      child: FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          return Stack(
            children: [
              widget.child,
              if (snapshot.connectionState != ConnectionState.done && snapshot.connectionState != ConnectionState.none)
                AbsorbPointer(
                  child: Center(
                    child: Material(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                      color: widget.backgroundColor ?? Colors.black54,
                      child: Padding(
                        padding: const EdgeInsets.all(32),
                        child: CircularProgressIndicator(strokeWidth: widget.strokeWidth, valueColor: widget.valueColor),
                      ),
                    ),
                  ),
                ),
            ],
          );
        },
      ),
    );
  }

  void _onFutureChanged(Future future) {
    if (_future == future) {
      return;
    }
    setState(() {
      _future = future;
    });
  }
}
