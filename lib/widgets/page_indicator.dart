import 'dart:math';

import 'package:flutter/material.dart';

class PageIndicator extends AnimatedWidget {
  final PageController controller;
  final int itemCount;
  final Color selectedColor, dotColor;
  final double? dotSpacing, dotSize;
  final ValueChanged<int>? onPageSelected;

  PageIndicator({
    Key? key,
    required this.controller,
    required this.itemCount,
    required this.selectedColor,
    required this.dotColor,
    this.dotSpacing = 12,
    this.dotSize = 6,
    this.onPageSelected,
  }) : super(key: key, listenable: controller);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List.generate(itemCount, _buildDot),
    );
  }

  Widget _buildDot(int index) {
    return SizedBox(
      width: dotSpacing,
      child: Center(
        child: Material(
          color: Color.lerp(selectedColor, dotColor, min(((controller.page ?? controller.initialPage) - index).abs().toDouble(), 1)),
          type: MaterialType.circle,
          child: SizedBox(
            width: dotSize,
            height: dotSize,
            child: InkWell(
              onTap: () => onPageSelected?.call(index),
            ),
          ),
        ),
      ),
    );
  }
}
