import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ProgressCachedNetworkImage extends StatelessWidget {
  final String image;
  final double strokeWidth;
  final double? width, height;
  final Widget? placeholder;
  final Animation<Color>? valueColor;

  const ProgressCachedNetworkImage({Key? key, required this.image, this.strokeWidth = 2, this.width, this.height, this.placeholder, this.valueColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: image,
      width: width,
      height: height,
      fit: BoxFit.cover,
      fadeInDuration: const Duration(milliseconds: 300),
      placeholder: (context, url) => SizedBox(
        width: width,
        height: height,
        child: placeholder ??
            Center(
              child: SizedBox(
                width: min(width ?? 20, 20),
                height: min(height ?? 20, 20),
                child: CircularProgressIndicator(strokeWidth: strokeWidth, valueColor: valueColor),
              ),
            ),
      ),
    );
  }
}
